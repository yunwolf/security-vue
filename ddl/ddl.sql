CREATE TABLE `sys_user`
(
    `id`              int(10)      NOT NULL AUTO_INCREMENT,
    `account_name`    varchar(255) NULL,
    `password`        varchar(255) NULL,
    `realname`        varchar(255) NULL,
    `sex`             varchar(255) NULL,
    `tel`             varchar(255) NULL,
    `photo`           varchar(255) NULL,
    `is_lock`         varchar(255) NULL,
    `salt`            varchar(255) NULL,
    `toke`            varchar(255) NULL,
    `create_time`     datetime(0)  NULL,
    `update_time`     datetime(0)  NULL,
    `last_login_time` datetime(0)  NULL,
    `create_user`     varchar(255) NULL,
    `update_user`     varchar(50)  NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `sys_menu`
(
    `id`            int(10)      NOT NULL AUTO_INCREMENT,
    `parent_id`     int(10)      NULL,
    `menu_node`     varchar(255) NULL,
    `menu_path`     varchar(255) NULL,
    `menu_name`     varchar(255) NULL,
    `menu_sort`     int(10)      NULL,
    `menu_icon`     varchar(255) NULL,
    `menu_type`     varchar(255) NULL,
    `create_time`   datetime(0)  NULL,
    `update_time`   datetime(0)  NULL,
    `is_enable`     varchar(255) NULL,
    `create_user`   varchar(255) NULL,
    `update_user`   datetime(0)  NULL,
    `describe_text` varchar(255) NULL,
    PRIMARY KEY (`id`)
);


CREATE TABLE `sys_role_menu`
(
    `id`          int(10)      NOT NULL,
    `role_id`     int(10)      NULL,
    `menu_id`     int(10)      NULL,
    `create_time` datetime(0)  NULL,
    `update_time` datetime(0)  NULL,
    `create_user` varchar(255) NULL,
    `update_user` varchar(255) NULL,
    PRIMARY KEY (`id`)
);


CREATE TABLE `sys_history`
(
    `id`             int(10)      NOT NULL AUTO_INCREMENT,
    `service_name`   varchar(255) NULL,
    `service_url`    varchar(255) NULL,
    `request_ip`     varchar(50)  NULL,
    `user_id`        int(10)      NULL,
    `run_status`     varchar(255) NULL,
    `consuming_time` datetime(0)  NULL,
    `create_time`    datetime(0)  NULL,
    `update_time`    datetime(0)  NULL,
    `service_type`   varchar(255) NULL,
    `http_method`    varchar(255) NULL,
    `ua`             varchar(255) NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `sys_role`
(
    `id`            int(10)      NOT NULL,
    `role_en`       varchar(255) NULL,
    `role_cn`       varchar(255) NULL,
    `create_time`   datetime(0)  NULL,
    `update_time`   datetime(0)  NULL,
    `describe_text` varchar(255) NULL,
    `is_enable`     varchar(255) NULL,
    `readonly`      varchar(255) NULL,
    `create_user`   varchar(255) NULL,
    `update_user`   varchar(255) NULL,
    PRIMARY KEY (`id`)
);


CREATE TABLE `sys_user_role`
(
    `id`          int(10)      NOT NULL AUTO_INCREMENT,
    `role_id`     int(10)      NULL,
    `user_id`     int(10)      NULL,
    `create_time` datetime(0)  NULL,
    `update_time` datetime(0)  NULL,
    `create_user` varchar(255) NULL,
    `update_user` varchar(255) NULL,
    PRIMARY KEY (`id`)
);