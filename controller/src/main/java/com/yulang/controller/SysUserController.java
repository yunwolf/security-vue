package com.yulang.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.yulang.common.pojo.SysUser;
import com.yulang.common.vo.CloudResult;
import com.yulang.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@RestController
@RequestMapping("/sys-user")
@Api(tags = "系统用户")
public class SysUserController extends ApiController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/get-one")
    @ApiOperation("查询测试")
    public CloudResult getOne(){
        return CloudResult.createBySuccess(sysUserService.getById("00"));
    }


}

