package com.yulang.common.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_history")
public class SysHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(SERVICE_NAME)
    private String serviceName;

    @TableField(SERVICE_URL)
    private String serviceUrl;

    @TableField(REQUEST_IP)
    private String requestIp;

    @TableField(USER_ID)
    private Integer userId;

    @TableField(RUN_STATUS)
    private String runStatus;

    @TableField(CONSUMING_TIME)
    private Date consumingTime;

    @TableField(CREATE_TIME)
    private Date createTime;

    @TableField(UPDATE_TIME)
    private Date updateTime;

    @TableField(SERVICE_TYPE)
    private String serviceType;

    @TableField(HTTP_METHOD)
    private String httpMethod;

    @TableField(UA)
    private String ua;


    public static final String ID = "`id`";

    public static final String SERVICE_NAME = "`service_name`";

    public static final String SERVICE_URL = "`service_url`";

    public static final String REQUEST_IP = "`request_ip`";

    public static final String USER_ID = "`user_id`";

    public static final String RUN_STATUS = "`run_status`";

    public static final String CONSUMING_TIME = "`consuming_time`";

    public static final String CREATE_TIME = "`create_time`";

    public static final String UPDATE_TIME = "`update_time`";

    public static final String SERVICE_TYPE = "`service_type`";

    public static final String HTTP_METHOD = "`http_method`";

    public static final String UA = "`ua`";


    public static final String ID_COMMENT = "";

    public static final String SERVICE_NAME_COMMENT = "";

    public static final String SERVICE_URL_COMMENT = "";

    public static final String REQUEST_IP_COMMENT = "";

    public static final String USER_ID_COMMENT = "";

    public static final String RUN_STATUS_COMMENT = "";

    public static final String CONSUMING_TIME_COMMENT = "";

    public static final String CREATE_TIME_COMMENT = "";

    public static final String UPDATE_TIME_COMMENT = "";

    public static final String SERVICE_TYPE_COMMENT = "";

    public static final String HTTP_METHOD_COMMENT = "";

    public static final String UA_COMMENT = "";



    public static SysHistory of(Object obj) {
        SysHistory e = new SysHistory();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<SysHistory> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
