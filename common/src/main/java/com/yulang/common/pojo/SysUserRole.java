package com.yulang.common.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user_role")
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(ROLE_ID)
    private Integer roleId;

    @TableField(USER_ID)
    private Integer userId;

    @TableField(CREATE_TIME)
    private Date createTime;

    @TableField(UPDATE_TIME)
    private Date updateTime;

    @TableField(CREATE_USER)
    private String createUser;

    @TableField(UPDATE_USER)
    private String updateUser;


    public static final String ID = "`id`";

    public static final String ROLE_ID = "`role_id`";

    public static final String USER_ID = "`user_id`";

    public static final String CREATE_TIME = "`create_time`";

    public static final String UPDATE_TIME = "`update_time`";

    public static final String CREATE_USER = "`create_user`";

    public static final String UPDATE_USER = "`update_user`";


    public static final String ID_COMMENT = "";

    public static final String ROLE_ID_COMMENT = "";

    public static final String USER_ID_COMMENT = "";

    public static final String CREATE_TIME_COMMENT = "";

    public static final String UPDATE_TIME_COMMENT = "";

    public static final String CREATE_USER_COMMENT = "";

    public static final String UPDATE_USER_COMMENT = "";



    public static SysUserRole of(Object obj) {
        SysUserRole e = new SysUserRole();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<SysUserRole> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
