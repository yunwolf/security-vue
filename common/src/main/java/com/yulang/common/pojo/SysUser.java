package com.yulang.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(ACCOUNT_NAME)
    private String accountName;

    @TableField(PASSWORD)
    private String password;

    @TableField(REALNAME)
    private String realname;

    @TableField(SEX)
    private String sex;

    @TableField(TEL)
    private String tel;

    @TableField(PHOTO)
    private String photo;

    @TableField(IS_LOCK)
    private String isLock;

    @TableField(SALT)
    private String salt;

    @TableField(TOKE)
    private String toke;

    @TableField(CREATE_TIME)
    private Date createTime;

    @TableField(UPDATE_TIME)
    private Date updateTime;

    @TableField(LAST_LOGIN_TIME)
    private Date lastLoginTime;

    @TableField(CREATE_USER)
    private String createUser;

    @TableField(UPDATE_USER)
    private String updateUser;


    public static final String ID = "`id`";

    public static final String ACCOUNT_NAME = "`account_name`";

    public static final String PASSWORD = "`password`";

    public static final String REALNAME = "`realname`";

    public static final String SEX = "`sex`";

    public static final String TEL = "`tel`";

    public static final String PHOTO = "`photo`";

    public static final String IS_LOCK = "`is_lock`";

    public static final String SALT = "`salt`";

    public static final String TOKE = "`toke`";

    public static final String CREATE_TIME = "`create_time`";

    public static final String UPDATE_TIME = "`update_time`";

    public static final String LAST_LOGIN_TIME = "`last_login_time`";

    public static final String CREATE_USER = "`create_user`";

    public static final String UPDATE_USER = "`update_user`";


    public static final String ID_COMMENT = "";

    public static final String ACCOUNT_NAME_COMMENT = "";

    public static final String PASSWORD_COMMENT = "";

    public static final String REALNAME_COMMENT = "";

    public static final String SEX_COMMENT = "";

    public static final String TEL_COMMENT = "";

    public static final String PHOTO_COMMENT = "";

    public static final String IS_LOCK_COMMENT = "";

    public static final String SALT_COMMENT = "";

    public static final String TOKE_COMMENT = "";

    public static final String CREATE_TIME_COMMENT = "";

    public static final String UPDATE_TIME_COMMENT = "";

    public static final String LAST_LOGIN_TIME_COMMENT = "";

    public static final String CREATE_USER_COMMENT = "";

    public static final String UPDATE_USER_COMMENT = "";



    public static SysUser of(Object obj) {
        SysUser e = new SysUser();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<SysUser> queryWrapper(Object obj) {
        return new QueryWrapper<SysUser>(of(obj));
    }

}
