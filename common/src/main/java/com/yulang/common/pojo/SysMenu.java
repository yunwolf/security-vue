package com.yulang.common.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_menu")
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(PARENT_ID)
    private Integer parentId;

    @TableField(MENU_NODE)
    private String menuNode;

    @TableField(MENU_PATH)
    private String menuPath;

    @TableField(MENU_NAME)
    private String menuName;

    @TableField(MENU_SORT)
    private Integer menuSort;

    @TableField(MENU_ICON)
    private String menuIcon;

    @TableField(MENU_TYPE)
    private String menuType;

    @TableField(CREATE_TIME)
    private Date createTime;

    @TableField(UPDATE_TIME)
    private Date updateTime;

    @TableField(IS_ENABLE)
    private String isEnable;

    @TableField(CREATE_USER)
    private String createUser;

    @TableField(UPDATE_USER)
    private Date updateUser;

    @TableField(DESCRIBE_TEXT)
    private String describeText;


    public static final String ID = "`id`";

    public static final String PARENT_ID = "`parent_id`";

    public static final String MENU_NODE = "`menu_node`";

    public static final String MENU_PATH = "`menu_path`";

    public static final String MENU_NAME = "`menu_name`";

    public static final String MENU_SORT = "`menu_sort`";

    public static final String MENU_ICON = "`menu_icon`";

    public static final String MENU_TYPE = "`menu_type`";

    public static final String CREATE_TIME = "`create_time`";

    public static final String UPDATE_TIME = "`update_time`";

    public static final String IS_ENABLE = "`is_enable`";

    public static final String CREATE_USER = "`create_user`";

    public static final String UPDATE_USER = "`update_user`";

    public static final String DESCRIBE_TEXT = "`describe_text`";


    public static final String ID_COMMENT = "";

    public static final String PARENT_ID_COMMENT = "";

    public static final String MENU_NODE_COMMENT = "";

    public static final String MENU_PATH_COMMENT = "";

    public static final String MENU_NAME_COMMENT = "";

    public static final String MENU_SORT_COMMENT = "";

    public static final String MENU_ICON_COMMENT = "";

    public static final String MENU_TYPE_COMMENT = "";

    public static final String CREATE_TIME_COMMENT = "";

    public static final String UPDATE_TIME_COMMENT = "";

    public static final String IS_ENABLE_COMMENT = "";

    public static final String CREATE_USER_COMMENT = "";

    public static final String UPDATE_USER_COMMENT = "";

    public static final String DESCRIBE_TEXT_COMMENT = "";



    public static SysMenu of(Object obj) {
        SysMenu e = new SysMenu();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<SysMenu> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
