package com.yulang.common.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.INPUT)
    private Integer id;

    @TableField(ROLE_EN)
    private String roleEn;

    @TableField(ROLE_CN)
    private String roleCn;

    @TableField(CREATE_TIME)
    private Date createTime;

    @TableField(UPDATE_TIME)
    private Date updateTime;

    @TableField(DESCRIBE_TEXT)
    private String describeText;

    @TableField(IS_ENABLE)
    private String isEnable;

    @TableField(READONLY)
    private String readonly;

    @TableField(CREATE_USER)
    private String createUser;

    @TableField(UPDATE_USER)
    private String updateUser;


    public static final String ID = "`id`";

    public static final String ROLE_EN = "`role_en`";

    public static final String ROLE_CN = "`role_cn`";

    public static final String CREATE_TIME = "`create_time`";

    public static final String UPDATE_TIME = "`update_time`";

    public static final String DESCRIBE_TEXT = "`describe_text`";

    public static final String IS_ENABLE = "`is_enable`";

    public static final String READONLY = "`readonly`";

    public static final String CREATE_USER = "`create_user`";

    public static final String UPDATE_USER = "`update_user`";


    public static final String ID_COMMENT = "";

    public static final String ROLE_EN_COMMENT = "";

    public static final String ROLE_CN_COMMENT = "";

    public static final String CREATE_TIME_COMMENT = "";

    public static final String UPDATE_TIME_COMMENT = "";

    public static final String DESCRIBE_TEXT_COMMENT = "";

    public static final String IS_ENABLE_COMMENT = "";

    public static final String READONLY_COMMENT = "";

    public static final String CREATE_USER_COMMENT = "";

    public static final String UPDATE_USER_COMMENT = "";



    public static SysRole of(Object obj) {
        SysRole e = new SysRole();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<SysRole> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
