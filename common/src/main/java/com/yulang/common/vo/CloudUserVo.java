package com.yulang.common.vo;

import com.yulang.common.pojo.SysRole;
import com.yulang.common.pojo.SysUser;
import lombok.Data;

@Data
public class CloudUserVo extends SysUser {


    private SysRole cloudRole;
    private  Long roleId;
    private  String roleName;
}
