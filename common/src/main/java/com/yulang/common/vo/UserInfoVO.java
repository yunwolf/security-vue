package com.yulang.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.google.common.collect.Sets;
import com.yulang.common.pojo.SysRole;
import com.yulang.common.pojo.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class UserInfoVO extends SysUser implements Serializable {

    private Set<String> menuList;
    private Set<String> permissionList;
    @TableField(exist = false)
    private SysRole role;

    private Set<String> roles = Sets.newHashSet();

    private Set<MenuVO> menus = Sets.newHashSet();



}
