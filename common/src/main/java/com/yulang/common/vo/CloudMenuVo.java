package com.yulang.common.vo;

import com.yulang.common.pojo.SysMenu;
import lombok.Data;

import java.util.List;
@Data
public class CloudMenuVo extends SysMenu {
    private List<CloudMenuVo> children;
    private List<CloudMenuVo> menuList;
}
