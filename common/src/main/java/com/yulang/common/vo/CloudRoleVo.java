package com.yulang.common.vo;

import com.yulang.common.pojo.SysRole;
import com.yulang.common.pojo.SysUser;
import lombok.Data;

import java.util.List;

@Data
public class CloudRoleVo extends SysRole {
    private List<SysUser> cloudUserList;
    private List<CloudRoleMenusVo> yunRoleMenus;
    private List<CloudMenuVo> yunMenusVo;
    private List<Long> checkedIds;
}
