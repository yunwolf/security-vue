package com.yulang.common.vo;

import com.google.common.collect.Lists;
import com.yulang.common.pojo.SysMenu;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  菜单
 */
@Data
public class MenuVO extends SysMenu implements Serializable {



    List<MenuVO> children = Lists.newArrayList();

}
