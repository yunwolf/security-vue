package com.yulang.common.vo;

import com.yulang.common.pojo.SysRoleMenu;
import lombok.Data;

import java.util.List;

@Data
public class CloudRoleMenusVo extends SysRoleMenu {
    private List<CloudMenuVo> yunMenus;

    private  List<String> children;
}
