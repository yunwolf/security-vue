package com.yulang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.common.pojo.SysRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
public interface SysRoleService extends IService<SysRole> {

}
