package com.yulang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.common.pojo.SysMenu;
import com.yulang.mapper.SysMenuMapper;
import com.yulang.service.SysMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
