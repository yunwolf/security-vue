package com.yulang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.common.pojo.SysHistory;
import com.yulang.mapper.SysHistoryMapper;
import com.yulang.service.SysHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Service
public class SysHistoryServiceImpl extends ServiceImpl<SysHistoryMapper, SysHistory> implements SysHistoryService {

}
