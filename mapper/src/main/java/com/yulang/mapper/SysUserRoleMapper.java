package com.yulang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.common.pojo.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
