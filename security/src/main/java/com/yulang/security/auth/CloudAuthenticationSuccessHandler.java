package com.yulang.security.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yulang.common.pojo.SysUser;
import com.yulang.common.utils.CloudResponseUtils;
import com.yulang.common.vo.CloudResult;
import com.yulang.common.vo.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  认证处理
 */
@Component
public class CloudAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private ObjectMapper objectMapper;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse response, Authentication auth) throws IOException, ServletException {


        SysUser user = new SysUser();
        SecurityUser securityUser = ((SecurityUser) auth.getPrincipal());
        user.setToke(securityUser.getLoginUser().getToke());
        Map map = new HashMap();
        map.put("result","success");
        map.put("loginToken",securityUser.getLoginUser().getToke());
        CloudResponseUtils.out(response, CloudResult.createByCodeSuccess(200,"登录成功",map));
    }
}
