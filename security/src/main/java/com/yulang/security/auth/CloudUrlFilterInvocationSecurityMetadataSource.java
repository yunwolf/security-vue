package com.yulang.security.auth;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yulang.common.pojo.SysMenu;
import com.yulang.common.pojo.SysRole;
import com.yulang.common.pojo.SysRoleMenu;
import com.yulang.mapper.SysMenuMapper;
import com.yulang.mapper.SysRoleMapper;
import com.yulang.mapper.SysRoleMenuMapper;
import com.yulang.security.set.IgnoreUrlConfig;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * 用户角色权限信息
 */
@Component
public class CloudUrlFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Resource
    SysMenuMapper cloudMenuMapper;
    @Resource
    SysRoleMenuMapper cloudRoleMenuMapper;
    @Resource
    SysRoleMapper cloudRoleMapper;
    @Resource
    IgnoreUrlConfig ignoreUrlConfig;

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {

        String requestUrl = ((FilterInvocation) object).getRequestUrl();
        //放行的url过滤
        for (String ignoreUrl : ignoreUrlConfig.getNocheck().getIgnoreUrls()) {
            if (ignoreUrl.equals(requestUrl)){
                return null;
            }
        }

        if (requestUrl.contains("/login")){
            return null;
        }

        // 查询数据库DB所有url
        List<SysMenu> permissionList = cloudMenuMapper.selectList(null);
        for (SysMenu permission : permissionList) {
            // 获取该url所对应的权限
            if ((permission.getMenuPath()).equals(requestUrl)) {
                List<SysRoleMenu> permissions = cloudRoleMenuMapper.selectList(new QueryWrapper<SysRoleMenu>().eq("menu_id", permission.getId()));
                List<String> roles = new LinkedList<>();
                if (!CollectionUtils.isEmpty(permissions)){
                    permissions.forEach( e -> {
                        Integer roleId = e.getRoleId();
                        SysRole role = cloudRoleMapper.selectById(roleId);
                        roles.add(role.getRoleEn());
                    });
                }
                // 角色-权限信息的集合处理
                return SecurityConfig.createList(roles.toArray(new String[roles.size()]));
            }
        }

        return SecurityConfig.createList("role_login");
    }


}
