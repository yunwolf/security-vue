package com.yulang.security;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yulang.common.pojo.SysRole;
import com.yulang.common.pojo.SysUser;
import com.yulang.common.pojo.SysUserRole;
import com.yulang.common.vo.SecurityUser;
import com.yulang.mapper.SysRoleMapper;
import com.yulang.mapper.SysUserMapper;
import com.yulang.mapper.SysUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * 认证用户
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper cloudUserMapper;  //用户mapper
    @Autowired
    private SysRoleMapper cloudRoleMapper; // 查询角色
    @Autowired
    private SysUserRoleMapper cloudUserRoleMapper; //查询用户和角色对应的关系 ，是否含有该角色

    /***
     * 根据账号获取用户信息
     * @param accountName:
     * @return: org.springframework.security.core.userdetails.UserDetails
     */
    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {
        // 从数据库中取出用户信息
        List<SysUser> userList = cloudUserMapper.selectList(new QueryWrapper<SysUser>().eq("account_name", accountName));
        SysUser user;
        // 判断用户是否存在
        if (!CollectionUtils.isEmpty(userList)) {
            user = userList.get(0);
        } else {
            throw new UsernameNotFoundException("用户名数据库未查到，请注册后再试！");
        }
        // 返回UserDetails实现类
        return new SecurityUser(user, getUserAndRole(Long.valueOf(user.getId())));
    }

    /***
     * 根据token获取用户权限
     *
     * @param token:
     */
    public SecurityUser checkAccountByToken(String token) {
        SysUser user = null;
        List<SysUser> loginList = cloudUserMapper.selectList(new QueryWrapper<SysUser>().eq("login_token", token));
        if (!CollectionUtils.isEmpty(loginList)) {
            user = loginList.get(0);
        }
        return user != null ? new SecurityUser(user, getUserAndRole(Long.valueOf(user.getId()))) : null;
    }

    /**
     *
     * 角色信息获取存储
     */
    private List<SysRole> getUserAndRole(Long  userId) {
        List<SysUserRole> userRoles = cloudUserRoleMapper.selectList(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        List<SysRole> roleList = new LinkedList<>();
        for (SysUserRole userRole : userRoles) {
            SysRole role = cloudRoleMapper.selectById(userRole.getRoleId());
            roleList.add(role);
        }
        return roleList;
    }

}
