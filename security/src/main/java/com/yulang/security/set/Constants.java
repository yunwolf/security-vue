package com.yulang.security.set;

import java.util.HashMap;
import java.util.Map;

/**
 *  日常工作中遇到的常量可以放在这里进行统一管理和使用
 *
 */
public class Constants {



    public static String SECURITY_SALT = "gaoxinqimeng";
    public static final int HASH_ITERATIONS = 1;
    public static final String REQUEST_HEADER = "auth-token";
    public static final String REQUEST_METHOD_POST = "POST";
    public static final String REQUEST_METHOD_GET = "GET";
}
